/*
 *  Header file for driving an RGB LED on a PIC16F1827
 *  The 16F1827 has 2 PWM pins in a row on A3 and A4,
 *  so those are the ones being used.
 *
 *  Only two of the pins can be PWMed.
 *  It was tested, and G gets us the most variety of colours,
 *  followed by B. R will just be on or off.
 */

// Pins to use for LED
#define R_PIN PIN_A2            // No PWM available
#define G_PIN PIN_A3            // CCPB3
#define B_PIN PIN_A4            // CCPB4

// Colour index defines
#define BLACK 0
#define RED 1
#define GREEN 2
#define BLUE 3
#define YELLOW 4
#define CYAN 5
#define PURPLE 6
#define ORANGE 7
#define WHITE 8

// Optional enum for defining colours instead of a string of #defines
//typedef enum {
//  BLACK, RED, GREEN, BLUE, YELLOW, CYAN, PURPLE, ORANGE, WHITE
//} Colour;

// Colour RGB values
// Red LED is much dimmer, so the green and blue need to have low values
uint8_t colours[][3] = {        // Defined as R[0-1], G[0-255], B[0-255]
  {0, 0, 0}                     // BLACK
  {1, 0, 0},                    // RED
  {0, 15, 0},                   // GREEN
  {0, 0, 15},                   // BLUE
  {1, 15, 0},                   // YELLOW
  {0, 15, 5},                   // CYAN
  {1, 0, 10},                   // PURPLE
  {1, 5, 0},                    // ORANGE
  {1, 15, 15},                  // WHITE
};


// Enum for state of the system
// Used to display LED patterns
typedef enum {
  NONE, PM_LOW, PM_MEDIUM, PM_HIGH
} State;

#define MAX_PATTERN_LEN 6  // Sets maximum number of colour changes in a pattern


// Struct for storing LED patterns
// `cycles` stores the time in multiples of 20ms for each colour change
// Maximum cycles 255
// `colours` stores the colour of each point in the pattern
// If `cycles[i]` is 0, the pattern has ended and should restart
typedef struct {
  uint8_t cycles[MAX_PATTERN_LEN];
  uint8_t colours[MAX_PATTERN_LEN];
} LEDPattern;


// Initialise LED patterns
// Multiples of 20ms cycle
LEDPattern led_patts[] = {
  // State NONE
  {{0}, {0}},
  // State PM_LOW
  {{0, 10, 250, 0}, {GREEN, BLACK, BLACK, 0}},
  // State PM_MEDIUM
  {{0, 10, 25, 35, 250, 0}, {ORANGE, BLACK, ORANGE, BLACK, BLACK, 0}},
  // State PM_HIGH
  {{0, 10, 25, 0}, {RED, BLACK, RED, 0}}
};


// Global variables to store the current state
State led_state = NONE;
uint8_t cycle_count;            // Keeps track of position in LED pattern


// Function prototypes
void setLEDColour(uint8_t colour);
void updateLED();
void setupLEDTimer(void);
