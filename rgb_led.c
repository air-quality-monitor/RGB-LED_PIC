/*
 *  Library for driving an RGB LED on a PIC16F1827
 *  The 16F1827 has 2 PWM pins in a row on A3 and A4,
 *  so those are the ones being used.
 *
 *  Only two of the pins can be PWMed.
 *  It was tested, and G gets us the most variety of colours,
 *  followed by B. R will just be on or off.
 */

#include "rgb_led.h"
#use pwm(frequency=1000, duty=50, output=PIN_A3) // Used for G
#use pwm(frequency=1000, duty=50, output=PIN_A4) // Used for B


void setLEDColour(uint8_t colour) {
  /*
   *  Set LED colour to a specific colour defined in `colours` array
   */

  output_bit(R_PIN, colours[colour][0]);
  set_pwm3_duty(colours[colour][1]);
  set_pwm4_duty(colours[colour][2]);
}


#INT_TIMER0
void updateLED() {
  /*
   *  Interrupt routine to update the RGB LED colour
   *  Colour based on the status - the current pattern playing
   */

  // Pointer to current pattern
  LEDPattern * patt = &led_patts[led_state];
  uint8_t current_colour = patt->colours[0];

  // Already set to first pattern colour - check the rest
  // Iterate through pattern elements and compare the intended
  // cycle count with cycle_count
  for (uint8_t i=1; i<MAX_PATTERN_LEN; i++) {
    // Restart pattern early when cycles[i] = 0
    if (patt->cycles[i] == 0) {
      current_colour = patt->colours[0];
      cycle_count = 0;
      break;
    }
    // Break loop when next colour change not reached yet
    else if (patt->cycles[i] > cycle_count) break;

    // Otherwise set current colour
    current_colour = patt->colours[i];
  }

  setLEDColour(current_colour);

  cycle_count++;

  // Set Timer0 count to make overflow period 20ms
  set_timer0(99);
}


void setupLEDTimer(void) {
  /*
   *  Set up Timer 0 to update LED every 20ms
   */
  setup_timer_0(T0_INTERNAL | T0_DIV_128); // Overflow every 32.64 ms
  enable_interrupts(INT_TIMER0);           // set_timer0(99) each overflow to set 20ms period
  enable_interrupts(GLOBAL);
}
