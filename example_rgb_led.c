/*
 *  RGB LED test using PIC16F1827
 *
 *  Wiring:
 *    R - RA2 - no PWM available
 *    G - RA3 / CCP3
 *    B - RA4 / CCP4
 */


#include <16F1827.h>

#fuses INTRC_IO,NOWDT,NOPROTECT,NOLVP,MCLR
#use delay(clock=8000000)

#include <stdint.h>
#include "rgb_led.c"  // defines color names and setLEDColour()


void main() {
  setupLEDTimer();

  while(1) {
    led_state = PM_LOW;
    cycle_count = 0;
    delay_ms(10000);
    led_state = PM_MEDIUM;
    cycle_count = 0;
    delay_ms(10000);
    led_state = PM_HIGH;
    cycle_count = 0;
    delay_ms(10000);
  }
}
